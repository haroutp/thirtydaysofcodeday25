﻿using System;
using System.Collections.Generic;
using System.IO;
class Solution {
    static void Main(String[] args) {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution */
        int n = int.Parse(Console.ReadLine());
        for (int i = 0; i < n; i++)
        {
            int val = int.Parse(Console.ReadLine());
            bool prime = PrimeOrNotv1(val);
            if(prime){
                Console.WriteLine("Prime");
            }else{
                Console.WriteLine("Not prime");
            }
        }
    }
    public static bool PrimeOrNotv1(int val){
        if(val <= 1){
            return false;
        }
        
        if(Math.Sqrt(val).GetType() == typeof(int)){
            return false;
        }
        for(int i = 2; i < Math.Sqrt(val) + 1; i ++){
            if(val == 2 && i == 2){
                continue;
            }
            if(val % i == 0){
                return false;
            }
        }
        return true;
    
            
    }
}

